.DEFAULT_GOAL := help
BUILD_DIR = $(PWD) # for subdirs
IN_DIR := ./src
OUT_DIR := ./out
OUT_NAME := mobile-phone-security
IN_FILES := $(IN_DIR)/*.md
IMAGES := $(shell find ./img -type f)
META := metadata.yml

PANDOC_CMD := pandoc $(IN_FILES) --fail-if-warnings --metadata-file=$(META) --from=markdown+grid_tables

.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

PDF := $(OUT_DIR)/$(OUT_NAME).pdf
$(PDF): Makefile $(OUT_DIR) $(META) $(IN_FILES)
	$(PANDOC_CMD) -o $(PDF)

.PHONY: pdf
pdf: $(PDF) ## Make the pdf

HTML := $(OUT_DIR)/$(OUT_NAME).html
$(HTML): Makefile $(OUT_DIR) $(META) $(IN_FILES)
	$(PANDOC_CMD) --standalone -o $(HTML)

.PHONY: html
html: $(HTML) ## Make the html

.PHONY: word-count
word-count: ## Count the words
	wc -w $(IN_FILES)

.PHONY: clean
clean:
	rm -rf $(OUT_DIR)

OPEN=$(word 1, $(wildcard /usr/bin/xdg-open /usr/bin/open))

.PHONY: open
open: $(PDF) ## Open the PDF
	$(OPEN) $(PDF)

.PHONY: open-html
open-html: $(HTML) ## Open the HTML
	$(OPEN) $(HTML)
