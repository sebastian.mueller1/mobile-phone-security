# Closing Remarks

Technology is not good or bad---at least most isn't.
It's not inherently liberatory or oppressive.
New tech creates new opportunities while closing off others.
With phones, this is no different.
Having access to instant communications and vast knowledge in our pockets is tremendously powerful, but it comes with the cost of increased surveillance.

You may think the State isn't surveilling you, but if you're involved in liberatory social movements---even loosely---it surely is.
Protecting yourself can protect your friends, family, or comrades who are move deeply involved in the movement.
You may think that the State is hacking your phone to tap your housing co-op's weekly meetings, but it almost certainly isn't.
Maximum security at all times is unattainable, and aiming for it is taxing.

After reading this you might be tempted to say "but they'll track me no matter what."
The belief that any level of safety against external threats is impossible is called security nihilism.
People who feel this often take one of two paths.
They can believe that no countermeasures work, so they keep acting and take no precautions thus creating a self-fulfilling prophecy ending with their arrest.
Or they can believe in the supremacy of the State and become paralyzed with inaction.
Repression works not just because of the stick that hits us or prison that cages us, but also because of the fear of those punishments and our subsequent self-imposed inaction.

Any steps you take can protect you, and many of them are so simple that you can start applying them right now.
At the easiest, you can avoid dragnet surveillance by using basic encrypted messenger apps and leaving your phone at home during demos or direct actions.
Every step you take beyond those will require your adversaries make more concerted efforts if they want to surveil or disrupt you.
Time and resources are limited, even for the large intelligence agencies.
Humans make mistakes, and computers break.
Your adversary is fallible, and you can significantly decrease the amount of data they can capture and what sort of insights they can glean from it.

Moreover, the State isn't always using the maximum theoretically possible surveillance methods.
Just because it's possible for the State to hack your phone or track it, they surely aren't doing that to catch you walking through parks after their closing hours.
Even in cases where the State wants to use maximum surveillance, they may do so ineptly.
Your threat model should account for the realistic expected response from your adversaries given their knowledge of your actions.

Learn about how the police and fascists in your area operate, and come up with a threat model for yourself and your crews.
Discuss it at length with your comrades.
Start with a few bits of OpSec knowledge and turn it into a security culture.
Foster shared understanding and practices that lead to increased security against the threats you're likely to face.
Take concrete steps, but make them pragmatic.
Start slow with just a few new things at a time until they become normalized, then build from there.
A plan is only good if you carry through with it, and trying to rush many large changes into a group tends to be overwhelming and frustrating.
Most successful plans are applied incrementally.

Beware of urban legends.
Activist spaces are rife with them, and security is no exception.
Ask "how?" and "why?" when people make claims about surveillance or countermeasures.
Base your threat model and your security plan on verifiable facts---or at least very probable conjectures with supporting evidence.

Use this knowledge to protect yourselves as you reshape the world.
