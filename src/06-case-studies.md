# Case Studies

To make the previous discussions more concrete, we're providing a number of case studies drawn from our experiences.
Some of these cases show individuals who already have more accurate threat models, and others who do not.
Some are based on urban legends, and others more on verifiable facts or very probable conjectures.
Where there are errors, they are discussed.

## Case 1: Planning Meetings for a Semi-Public Action

### Scenario

A collective is planning an occupation that will be kept secret until it begins, after which it will be made public via social media.
Planning primarily happens at in-person meetings at a local social center.

### Assumptions

The collective assumes that police are interested in preventing occupations, and the activists may be under surveillance.
This surveillance includes, but is not limited to, State-backed malware that could be on the individuals' phones.

### Countermeasures

To prevent the State from using the phones' microphones to record the meeting, phones are collected and placed in a sealed plastic box in an adjacent room.

### Analysis

It is correct to say that the phones could have been compromised by malware, and it may be correct to say that moving the phones to another room impedes their microphones in recording the conversations.
However, there is an assumption about how effective moving the phones out of the room is, and this could be verified by starting a recording, placing the phone in the box, then having a loud conversation to see how much is understandable.
If the voices are remotely recognizable, snippets could be recovered with audio editing software.

If the social center is not regularly swept for microphones or other bugs, the conversations may still be recorded.
If the group or other groups who frequent the social center are heavily surveilled, laser microphones placed in nearby buildings could record the conversations.

If individuals are under passive surveillance, the fact that a meeting took place and who attended could still be revealed by the repeated presence of the same group of phones at fixed location from 19:00 to 21:00 on Wednesdays for many consecutive weeks.

### Recommendations

If phones are collected to prevent surveillance, they should also be powered off.
There should be loud, ambient noise where phones are placed to minimize the chances that they can pick up the audio of the conversations.

If the group believes they may be prosecuted for conspiracy to commit a crime, the may want to leave phones at home or power them off before traveling to the meeting.
This can be further minimized by not bringing the same phones to the action itself.

If high security is desired, bugging of the room or recording via State actors can be further reduced by meeting at locations that are not affiliated with liberatory movements.
If the group wants to meet at a central, known location for convenience, it should be established at the start of the meeting that only the current action (and nothing more illegal) should be discussed.

## Case 2: Overheard Chatter

### Scenario

Some members of an affinity group are hanging out in a park to socialize, not to plan an action.
Their phones are present and on, but their security culture includes not discussing past actions or trading war stories as these can contain incriminating information.

### Assumptions

The group has assumed that the police only want to listen to their conversations if they are about past or future illegal activities.
They have assumed that their everyday conversations are uninteresting and uninformative.

### Countermeasures

The group has taken no countermeasures against their conversations being overheard.

### Analysis

If the group is consciously not discussing plans or past actions, then obviously no microphone can overhear what isn't said aloud.
However, planned and carried-out actions aren't the only thing the State is interested in.
Gossip, drama, love interests, social ties, and even the dispositions of people and orgs within a milieu toward one another are valuable intel.
This can allow the State to create more accurate social maps.
If the State suspects one individual was involved in something they are investigating, and they know the individual had accomplices, using social maps that are constructed from bit of casuals conversation can help them narrow down their list of suspects or reveal the members of an affinity group.
Such overheard conversations can give the State insight about who is feeling ostracized and resentful so that they can be targeted to become an informant.
Small conflicts and be exploited, and heated emotions can be fanned into roaring disputes.

### Recommendations

There is a generational split among activists between those who organized before the widespread use of mobiles phones and those who began organize after the ubiquity of phones.
There is a also a further split between those who organized using simple phones before the popularity of smart phones, and those who have always organized in a world where nearly all of their contacts have smart phones.
This gap is notable by the ability to make plans on the assumption the other people wouldn't have phones such as fixing locations and times with less spontaneous changes.
Additionally, those who organized prior to the adoption of mobile phones have a more acute sense of what it was like for organizing to increasingly take place where everyone effectively had microphones present.

As mentioned earlier in this zine, smart phones allow us to instantly communicate and have limitless information on hand at all times.
This comes with the cost of new avenues for surveillance.
Activists should be mindful that mobile phones present in homes, cars, and social settings might be gathering soft intelligence on social groups.
If we were to make the recommendation that phones should be more frequently powered off, we might be laughed at for paranoia or for the impracticality of the suggestion.
So-called liberal democracy gives the illusion that we do not live under a repressive police state, yet there are many cases where innocuous social circles and activists groups are hacked and surveilled, not to mention the more radical and involved groups.

Our suggestion is not that we should never have phones on our persons, but we do want to suggest that everyone become more aware of the effort the State expends to surveil and the utility of the information gleaned from casual conversation.
There may come a time when repression heightens and we begin to feel its presence more sharply.
To prepare for such times and to build habits that enable us to resist such repression, our suggestion is more moderate.
Practice heightened security starting now.
See if you can organize phoneless events.
When you hang out or go hiking, even if you meet at a pub, see if you can get everyone to leave their phones at home.
Accustom yourself to their absence.
Feel the freedom of knowing you're not leaking location data to the State and that no one can hear your conversation except for those present.

## Case 3: Squatting and Simple Phones

### Scenario

A crew of activist want to squat an unoccupied building with the goals of drawing attention to speculative property investments, and if the squatting is successful, turn it in to free housing for locals who have recently been evicted.
One team will be in the building doing the occupation, and other teams will be on the ground out front negotiating with the State and posting to social media.

### Assumptions

The occupation team thinks that the police could learn their identities by seeing what phones are communicating from within the building, and even if they are not arrested or prosecuted for this action, that knowledge could be used against them in the future.

### Countermeasures

To reduce the chances of their identities being learned in the case where they are not arrested during the action, the occupation team has chosen to not bring their personal phones.
They will only bring one "burner phone" to have comms with the negotiation team so that they can be involved with the decisions, to send posts to the social media team, and to have a sense of security instead of being isolated until the end of the action.
They will be using a phone with a SIM that is not registered to any of their names to make themselves anonymous.

### Analysis

The crew is correct in not brining personal phones into the building the are occupying as they could be used to identify them.
Police could do this by looking at what phones are in the building, and seeing to whom they are registered or looking at where they tend to spend the most time (e.g., when their user is at home sleeping).
The crew is incorrect in calling the phone a burner phone as its repeated use can be used to tie it back to the crew and individuals.
This phone is more accurately described as a demo phone.
Since some of the crew is remaining outside of the building without masks, the crew's identity is known even if not all of the identities of the occupation team are known.
If the phone is a personal "burner" belonging to one activist, and this phone has been turned on at the activist's home, this could be used to prove that the activist was inside the building or was involved.

The crew have overlooked the security implications of using the basic phone to communicate with the negotiations team.
The police may deploy an IMSI catcher so that they can read the SMS messages sent back and forth between the occupation team and the negotiation team.
This may give the police a leg up in the negotiations or give them opportunities to exploit division within the crew to allow them to more easily force an eviction.

However, if the police are likely to expend this level of effort on tracking down the individuals based off of the phones present in the occupation, there is likely a strong enough push for "law and order" that the occupation itself would not even be a viable action.

### Recommendations

The reasons the occupation team wanted to bring a single phone into the building were legitimate, but they should have used a demo phone with a single use account account they created on an E2EE chat app.
This account should only communicate with one anonymous account belonging to the teams outside to prevent leaking the crew's social network if the phone is confiscated or the app developer has data for those accounts that is later subpoenaed in court.

## Case 4: Simple Phone + Signal Desktop

### Scenario

Ruben is an activist involved with a crew he believes is under active surveillance due to their anti-government stances.
To minimize how much intelligence agencies and local police can track him, he uses a basic phone with a SIM card when he is out and about.
Because some discussions with his crew are more sensitive, they need an encrypted messenger and have chosen Signal.
Signal requires registering with a phone number and will only generate the initial encryption keys on the iOS and Android apps.
To get the Signal desktop app to work on his laptop, he has used the SIM card from his basic phone in his friend's smart phone to set up an initial key pair that he could link with his desktop app.
Afterwards, Ruben signs out of his account on the Signal app on his friend's phone.

### Assumptions

Ruben's decision to not carry a smart phone is based on the belief that smart phones are more trackable than basic phones.
Ruben also assumes that Signal is more secure than telephone calls or SMS, so he uses Signal for some of his communications.

### Countermeasures

Ruben's decision to use a basic phone is intended to minimize location tracking from his smart phone.
His decision to use Signal desktop is intended to prevent interception of his sensitive messages with comrades

### Analysis

Ruben's location is roughly as equally trackable when using a basic phone as with a smart phone.
His communications are more insecure because he does not have the possibility that "emergency" messages can be sent to or received from members of his crew using his basic phone, and if he does, they will be intercepted and stored by the State.
His countermeasures against surveillance have created a burden for both himself and his crew, and they have not made him more meaningfully secure against the threats he faces.

### Recommendations

Ruben should use his own smart phone for communications in general.
If there are times when he needs his location to be hidden or his conversations to not be eavesdropped on, he should leave his phone at home.

## Case 5: Phoneless Planning

### Scenario

The members of an affinity group have been actively involved in the liberatory movements for long enough that they are known to the State.
They are currently planning Something Big.
They have a ban on discussing it over electronic means, and they only discuss it in person.

### Assumptions

They have assumed that the State would go to great lengths to prevent their action and even greater length to investigate it after it occurs.
They assume it's possible that their electronics have been compromised by State malware.
They assume that even absent any evidence, they will be on the list of primary suspects for the action, so their OpSec for the action needs to be airtight.

### Countermeasures

Because of the possibility of malware, they are treating their electronics as less-than-trusted.
Because of the possibility of targeted investigations, they do not discuss their action in their homes, their vehicles, or known social centers and spaces tied to liberatory movements.
To help reduce metadata that links them together, they turn their phones off before they arrive at their meeting locations and turn them on again only after they've left.

### Analysis

The group is right to assume they may be under targeted surveillance, and they are right to treat their phones as snitches.
Turning their phones off does decrease the possibility of malware using a microphone to spy on them, and it does create some deniability about their locations during the meetings.
But this absence of information may be abnormal compared to their regular phone usage, and all of their phones disappearing roughly at the same time around a location could be a hint to the State that during these gaps something noteworthy is happening.
This could create incentive for additional surveillance such as bugging the location---if they use the same one repeatedly---or sending a plainclothes spook to wear a wire and follow them in to the cafe or bar where they meet.
Moreover, if one member of the group is caught but says nothing during interrogation, police could look at their phone records for anomalies.
The police could query the data by asking the questions: At the times when this phone went off, what other phones went off near it?
And what were the phones of our other suspects doing at that time?
This could reveal the rest of the members of the affinity group, or provide supporting evidence that members of the affinity group were the individual's accomplices.
It is possible the police do not think to ask these questions or that this is not part of standard operations, but it is better to leave no trail.

### Recommendations

Because they are anticipating targeted surveillance and resources to investigate their activities, they should leave all electronics at home and pick random locations for their meetings that are either very loud or very isolated.

## Case 6: Phones at Mass Actions

### Scenario

Isa is an activist who primarily attends larger demonstrations, and while she is not radical herself, she has some friends who are, and she is generally aware of what they do.
Fascists have planned a march, and Isa and some friends are going to join the crowds who hope to block their planned route.
In order to connect with her friends and get up-to-date information about the blockades or changing routes, Isa will bring her everyday phone (the only one she has).

### Assumptions

Isa is not worried about arrest because at similar actions in the past, when a large crowd of people who do not appear to be classically antifa block the streets, police only kettle them or drag them out of the way before rerouting the fascists.
She does not think that if she was arrested they would look through her phone at all, legally or illegally.
She is also not concerned about her phone's location data.

### Countermeasures

Isa has taken no countermeasures against her location data being collected or phone being confiscated.

### Analysis

At mass actions, police may use IMSI catchers to see who has attended these protests so they can build profiles.
This location data may be used to prosecute people for rioting even if the charges don't lead to conviction.

If Isa is arrested, which still may happen if the blockades are too small or she's one of the unlucky ones to get snatched while they form on the streets, she may have her phone searched.
From this, police may learn of her social network or the activities of her more radial friends.
This can endanger them more than her.

### Recommendations

Even though Isa does not anticipate arrest, she should be more cautious with her phone.
She could agree to meet with friends at a fixed place and time before the demo so that they can avoid bringing their phones at all, or if they really want to have real-time information, only one person in her group should bring a phone.
Being careful with her phone can protect her radical friends who might engage in more militant means of resistance to the fascists.

However, the chances of any of these things happening is low, and the perceived benefit of bringing a phone is high.
That makes this a case where it's "fine" for Isa to bring her phone... until suddenly it isn't.

## Case 7: General Planning and Communications

### Scenario

A collective organizes legal protests and hands our flyers promoting green and ecological alternatives to the current status quo such as going vegan, better funding for bicycle infrastructure, and decreased reliance on personal automobiles.
They use an email list hosted on a server provided by some local techy activists.

### Assumptions

The collective assumes that police are generally interested in activists, but that the collective itself is not specifically being targeted.
They know that local trolls like to harass the "hippie communists."
They also know that there are other more militant green organizations in their region, and that members of their collective may be in all manner of other groups.

### Countermeasures

The collective wants to avoid harassment, so they keep their email list private and invite only.
They want to avoid tracking by large email providers, so they self-host their email.

### Analysis

Email lists are largely popular because everyone has access to email, but there are many different chat apps and not everyone uses the same ones, so collectives tend to continue using email lists.
Often people claim to have insufficient room on their phones for more apps.
Some members of collectives have low technical skills and do not want to learn other apps, so sometimes email is unavoidable.

Eco-activists worldwide, including in western so-called democracies, are specifically targeted for surveillance even when not engaging in direct action.
Self-hosting the email list may decrease corporate surveillance, but there is always some weak link with subpoenas for data.
A large provider might comply without notifying the collective, and while the techies who run the server for the collective would likely quietly let them know even if they had a gag order, the police could circumvent this by going directly to the server's hosting company and subpoenaing them.
Further, the techies might not have the technical competence of large email provider to keep the server secure or even notice if it gets hacked by trolls or the State.

### Recommendations

If space on the phone is an issue, the activists should backup their photos and videos, then delete them to make space.
This is generally a good practice to help save data in case the phone is lost or breaks.

The collective should ideally move to an encrypted chat app, but if they continue to use email, it should only be for the most basic details such as the times and location of their activities.
Planning, internal debates, and significant discussion should stay off email as this information can give the State great insight into the collective which can be used for disruption.

## Case 8: Underground Raves

### Scenario

A collective plans underground outdoor raves during the corona virus pandemic.
They ask people to wear masks, and think this is sufficiently safe with regards to spreading the virus.
Police have a blanket ban on mass gatherings (except for work and other things that keep the machine of capital greased).

### Assumptions

The State has made active efforts to break up mass gatherings (of course, only of the working class and marginalized), but they will likely not retroactively look for evidence of these gatherings.
They have the ability to gather real-time phone location data which they may use to detect several hundred people in a remote location.
Police have informants who listen for things like this, and some people just like to snitch on others if they hear about something they don't like.

### Countermeasures

The rave is not posted to social media, and information is requested to be forwarded to additional contacts only via secure means.
In the info, people are requested to put their phones in airplane mode when they get near the designated location.

### Analysis

Not publicizing the event is an obviously correct step to keep police from learning about it on their own.
Asking people to only spread the info to trusted contacts in secure ways is also a great way to reduce risk, but all it takes it one person to forward a trimmed down message with just the location and time for the warning to be lost.
Even if the collective knows this, it is a risk they must accept.

### Recommendations

There is little the collective can to do prevent people from arriving with their phones left on, and there is little they can do to ensure the message stays only on the trusted and security-minded parts of the social web.
This is a hard problem in security culture because the lack of OpSec for a fraction of the individuals can still take down the whole group, especially since the individual benefit of keeping a phone on is high but the risk to the individual is low.
The organizers who put it on and brought the equipment are the most likely to face consequences.
If the crowd scatters during a raid, they will likely avoid consequences.
The best the collective can do is try to use shame before and during the event to convince people that their actions might be responsible for ruining the rave for everyone.

## Case 9: Dealing With a Weak Link

### Scenario

An affinity group targets nazis who harass people in their community.
They have an agreement of not bringing phones to their late night actions.
Felix, one of their most active members, thinks this is overly paranoid and refuses to leave his at home.

### Assumptions

The group has assumed that the State might use phone location data to investigate their activities.
They also assume that Felix bringing his phone endangers them all.

### Countermeasures

To prevent Felix from endangering them, the have put a hold on their activities until they can reach an agreement with Felix.

### Analysis

Felix's actions do endanger the group, and the group is right that they should not let him participate in their actions.
If the group fully ceases their actions, more harm could come to their community, and the risk of arrest from phones could be quite low depending on how police investigate things in the group's region.

### Recommendations

The affinity group could create a subgroup of members who agree to not bring phones to actions and continue their work.
In parallel, they could work with Felix to get him to understand how and why brining his phone creates unnecessary risk.
They could discuss with him that his actions cause them discomfort and that his actions do not affect him alone.
The group may be able to remain comrades with Felix, but they may need to exclude him from secret actions if he refuses to leave his phone at home.
