# Making a Plan

We cannot pretend to know your threat model, and we cannot address every bit of nuance for every region and situation.
What we can do is list some guidelines that are generally applicable.
When reading these, you need to consider what is practical.
What can you actually do?
And what will people in your social circle do?
Your new plan doesn't have to be perfect.
It just has to be better than whatever you're doing now.
If this means making compromises on security so you can continue to organize, you may have do that.
But at the same time, don't let others' poor security endanger you.
Find a balance.

This is by no means an exhaustive list, but are some ways to develop personal OpSec and group security culture:

- Use a smart phone as they are more secure against most threats activists face than simple phones.
- Do not bring your phone to activities that might interest police, and in particular protests that might be rowdy.
- Prefer E2EE encrypted apps for communication, enable disappearing messages, and avoid email.
- Use a password to unlock your phone, and enable device encryption.
- Disable fingerprint unlock on your phone before going to bed or leaving it unattended.
- Regularly back up photos and other data to an encrypted drive and remove them from your phone.
- Delete old data: DMs, group chat, emails, calendar events, etc.
- Leave group chats where you do not need to be present, and remove inactive members from group chats.
- Practice leaving your phone at home or powering it off when running errands or for small actions to habituate yourself to its absence.
- Start all meetings by establishing whether or not electronics are permitted.
  If not, power them off, collect them, and move them out of range of your conversation.

And most importantly:

> **Do not send messages or make voice calls about highly sensitive matters.
> Do not photograph or film incriminating things.
> Do not create evidence that can be used against you or others.**

## Void Where Prohibited

What was written here, and even the rest of this zine, are guidelines.
They may not apply to you.
In particular, digital security can leave particularly noticeable trails.
If Signal is very uncommon in your region, it's use could make you a target.
VPNs may be criminalized.
Use of Tor may get your a visit from the police.
The presence of secure communication apps on your phone could turn your arrest into a disappearance.
Before you download anything, do research about repression in your region to determine if the guidelines we have provided will make you safer or if they will endanger you.

## Alternatives

It is always easier to say "do this instead" rather than "don't do that," and when trying to change behavior or practices, providing alternatives increases the chances that someone will drop the old, insecure behavior.
There are legitimate reasons to have phones, and alternatives can mean less burden when we give up our phones or change our habits.

Barriers to getting rid of phones is that people want to have info, gather info, and trade contact info.
A pen and a note pad and let you have your collective's meeting minutes accessible in an analog manner.
You can use it to trade contact info, and if you're slick, you can carry a copy of your device's cryptographic fingerprint to establish a secure line even when you and the other party do not have your phones on you.
A paper calendar can allow you to schedule.
Printing out paper maps of the area of operations for an action can help you navigate.
If you create paper copies of information, ensure you promptly and securely dispose of it to avoid creating a literal paper trail of your activities.

## Phoneless Contingencies

While your plan may work today, it must also be forward thinking.
You may rely heavily on your phone for organizing while accepting the security risks, but there may come a time when repression or catastrophe disables your phones or the internet.
It is common during heightened repression for the State to cut mobile phone service or the internet for entire regions.
If your ability to organize and your safety relies on nearly everyone having phones and working internet, you are setting yourself up for certain modes of failure.
Word of mouth and the so-called sneakernet are fallbacks, and your planning needs to incorporate the possibility that this will the only way to move information.
